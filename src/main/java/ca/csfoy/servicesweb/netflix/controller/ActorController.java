package ca.csfoy.servicesweb.netflix.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;

import ca.csfoy.servicesweb.netflix.api.ActorDto;
import ca.csfoy.servicesweb.netflix.api.validation.ValidUUID;
import ca.csfoy.servicesweb.netflix.controller.errors.InvalidInputException;
import ca.csfoy.servicesweb.netflix.domaine.actor.ActorRepository;

@Component
@Validated
public class ActorController implements GraphQLQueryResolver {

	private final ActorRepository repository;
	private final ActorConverter converter;

	public ActorController(ActorRepository repository, ActorConverter converter) {
		this.repository = repository;
		this.converter = converter;
	}

	public void createActor(ActorDto actor) {		
		if (Objects.nonNull(actor)) {
			repository.create(converter.toActorCreation(actor));
		} else {
			throw new InvalidInputException("Invalid input for movie creation.");
		}
	}

	public ActorDto getActorById(@ValidUUID String id) {
		return converter.fromActor(repository.getBy(id));
	}

	public List<ActorDto> getActors() {
		return converter.fromActorList(repository.getAll());
	}

	public void updateActor(ActorDto actor) {
		repository.save(converter.toActor(actor));
	}

	public void deleteActor(String id) {
		repository.delete(id);
	}
}
