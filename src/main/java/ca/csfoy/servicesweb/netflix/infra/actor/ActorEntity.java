package ca.csfoy.servicesweb.netflix.infra.actor;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ActorEntity {
	
	@Id
	public String id;
	@Column(length=50, nullable=false)
	public String firstname;
	@Column(length=50, nullable=false)
	public String lastname;
	@Column(nullable=true)
	public LocalDate birthdate;
	@Column(nullable=true)
	public String bio;
	
	ActorEntity() {		
	}
	
	public ActorEntity(String id, String firstname, String lastname, LocalDate birthdate, String bio) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthdate = birthdate;
		this.bio = bio;
	}
}
