package ca.csfoy.servicesweb.netflix.api;

public class ActorDto {

	public static final String ACTOR_NOT_NULL_MESSAGE = "The birthdate must be in the past.";	
	public static final String BIRTHDATE_PAST = "The birthdate must be in the past.";
	
	public final String id;
	public final String firstname;
	public final String lastname;
	public final String birthdate;
	public final String bio; 
	
	public ActorDto(String id, String firstname, String lastname, String birthdate, String bio) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthdate = birthdate;
		this.bio = bio;
	}
}
