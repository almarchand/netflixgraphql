package ca.csfoy.servicesweb.netflix.api.actor;

import java.time.LocalDate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import ca.csfoy.servicesweb.netflix.api.ActorDto;
import ca.csfoy.servicesweb.netflix.controller.ActorConverter;
import ca.csfoy.servicesweb.netflix.domaine.actor.Actor;
import ca.csfoy.servicesweb.netflix.domaine.actor.ActorRepository;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("Api")
@SpringBootTest
@AutoConfigureMockMvc
public class ActorResourceTest {
	private static final String PATH_TO_TEST = "/graphql/actors";
    private static final String PATH_WITH_ID = "/123e4567-e89b-12d3-a456-426614174000";
    private static final String ANY_ID = "123e4567-e89b-12d3-a456-426614174000";
    private static final String ANY_FIRSTNAME = "Scarlett";
    private static final String ANY_LASTNAME = "Johansson";
    private static final LocalDate ANY_BIRTHDATE = LocalDate.now().minusYears(28);
    private static final String ANY_BIO = "Bio";
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@MockBean
	private ActorRepository repo;
	
	@MockBean 
	private ActorConverter converter;
	
	/* Ne fonctionne pas :(
	@Test
	public void get_ifGetSuccessful_thenReturn200Ok() throws Exception {
        Actor actor = new Actor(ANY_ID, ANY_FIRSTNAME, ANY_LASTNAME, ANY_BIRTHDATE, ANY_BIO);
        ActorDto dto1 = new ActorDto(ANY_ID, ANY_FIRSTNAME, ANY_LASTNAME, ANY_BIRTHDATE.toString(), ANY_BIO);
        Mockito.when(repo.getBy(ANY_ID)).thenReturn(actor);
        Mockito.when(converter.fromActor(actor)).thenReturn(dto1);
        
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/graphql")
        		.content("{\"query \"{\"getActorById(id: " + ANY_ID + ") }}")
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk())              
                .andReturn();        

        String responseAsString = result.getResponse().getContentAsString();
        String expectedBody = objectMapper.writeValueAsString(dto1);
        Assertions.assertEquals(expectedBody, responseAsString);
    }
	
	@Test
	public void get_ifActorDoesNotExist_thenReturn404Ok() throws Exception {
	}*/
}
