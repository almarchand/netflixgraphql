# Netflix GraphQL API

NetflixGraphQLApi est un projet de départ pour le récit utilisateur 8 du travail pratique 3 dans le cours de Services Web.  Il ne contient aucune gestion d'erreur, aucune validation, ni aucune sécurité.  De plus, il n'est pas 'fonctionnel' puisqu'aucun schéma GraphQL n'est présent dans le projet (Il n'y a pas de ressources REST non plus).

## Installation

Faire un clone/fork du projet pour obtenir une copie locale.

## License
[GNU](https://choosealicense.com/licenses/gpl-3.0/)
